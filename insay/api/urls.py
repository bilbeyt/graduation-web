from django.urls import path
from api.views import CountUpdateView


urlpatterns = [
    path('<slug>/', CountUpdateView.as_view()),
]