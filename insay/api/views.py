from rest_framework.generics import UpdateAPIView
from api.serializers import DeviceSerializer
from counter.models import Device
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class CountUpdateView(UpdateAPIView):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]
    lookup_field = "slug"


