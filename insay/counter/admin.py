from django.contrib import admin
from counter.models import Device, Location


class DeviceAdmin(admin.ModelAdmin):
    list_display = ["alias", "location", "in_count", "total_count", "is_active", "created_at", "slug"]
    list_filter = ["location", "created_at", "is_active"]
    search_fields = ["alias", "location"]
    #readonly_fields = ["in_count", "total_count"]
    exclude = ["slug", "group_name"]


class LocationAdmin(admin.ModelAdmin):
    list_display = ["name", "created_at"]
    exclude = ["slug"]


admin.site.register(Device, DeviceAdmin)
admin.site.register(Location, LocationAdmin)
