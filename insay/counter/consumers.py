from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from counter.models import Device
import json


class CounterConsumer(WebsocketConsumer):
    def websocket_connect(self, message):
        self.slug = self.scope["url_route"]["kwargs"]["slug"]
        self.device = Device.objects.get(slug=self.slug)
        self.base_send({"type": "websocket.accept"})
        async_to_sync(self.channel_layer.group_add)(self.device.group_name, self.channel_name)

    def receive(self, text_data):
        async_to_sync(self.channel_layer.group_send)(self.device.group_name, text_data)

    def message(self, text_data):
        self.send(text_data=json.dumps(text_data["message"]))

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.device.group_name, self.channel_name)