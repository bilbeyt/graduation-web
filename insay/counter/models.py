from django.db import models
from django.dispatch import receiver
from django.template.defaultfilters import slugify
from django.db.models.signals import pre_save
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import uuid
import json


class Location(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = models.SlugField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Device(models.Model):
    alias = models.CharField(max_length=100, unique=True)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    group_name = models.CharField(max_length=100)
    in_count = models.PositiveIntegerField(default=0)
    total_count = models.IntegerField(default=0)
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.alias


@receiver(pre_save, sender=Device)
def device_slug_handler(instance, sender, *args, **kwargs):
    instance.slug = slugify(instance.alias) 
    instance.group_name = "device_" + str(instance.id)

@receiver(pre_save, sender=Device)
def device_message_handler(instance, sender, *args, **kwargs):
    channel_layer = get_channel_layer()
    data = {
        "in_count": instance.in_count,
        "total_count": instance.total_count
    }
    message = {
        "type": "message",
        "message": json.dumps(data)
    }
    async_to_sync(channel_layer.group_send)(instance.group_name, message)


@receiver(pre_save, sender=Location)
def location_slug_handler(instance, sender, *args, **kwargs):
    instance.slug = slugify(instance.name) 
