from counter.consumers import CounterConsumer
from django.conf.urls import url

websocket_urlpatterns = [
    url(r'^(?P<location_slug>[-_\w]+)/(?P<slug>[-_\w]+)/$', CounterConsumer),
]