from django.urls import path
from counter.views import DeviceListView, DeviceDetailView, LocationListView


urlpatterns = [
    path('', LocationListView.as_view(), name="location_list"),
    path('<location>/', DeviceListView.as_view(), name="device_list"),
    path('<location>/<slug>/', DeviceDetailView.as_view(), name="device_detail")
]