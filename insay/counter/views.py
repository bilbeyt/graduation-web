from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from counter.models import Device, Location


class LocationListView(ListView):
    template_name = "counter/location_list.html"
    model = Location


class DeviceListView(ListView):
    template_name = "counter/device_list.html"
    model = Device


class DeviceDetailView(DetailView):
    template_name = "counter/device_detail.html"
    model = Device


